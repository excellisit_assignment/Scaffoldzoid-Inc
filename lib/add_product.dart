import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scaffoldzoid/db_model.dart';

import 'boxes.dart';
import 'navigation_utils.dart';

class AddProduct extends StatefulWidget {
  String storeName;
  AddProduct({Key? key, required this.storeName}) : super(key: key);

  @override
  State<AddProduct> createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  late final user = FirebaseAuth.instance.currentUser!;

  TextEditingController nameController = TextEditingController();
  TextEditingController rateController = TextEditingController();
  String path = "";
  final picker = ImagePicker();
  PickedFile? imageFile;
  String? imageBase64;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Orange"),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.all(8.0),
        children: [
          Container(
            margin: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: nameController,
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Product Name',
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(8.0),
            child: TextFormField(
              keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
              controller: rateController,
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Rate',
              ),
            ),
          ),
          Row(
            children: [
              InkWell(
                  child: Container(
                    width: 130,
                    height: 130,
                    padding: const EdgeInsets.all(8.0),
                    child: const Icon(
                      Icons.add_a_photo,
                      size: 50,
                    ),
                  ),
                  onTap: () {
                    getImage(ImageSource.camera);
                  }),
              const SizedBox(width: 10),
              InkWell(
                child: Container(
                  width: 130,
                  height: 130,
                  padding: const EdgeInsets.all(8.0),
                  child: const Icon(
                    Icons.collections,
                    size: 50,
                  ),
                ),
                onTap: () {
                  getImage(ImageSource.gallery);
                },
              ),
              Stack(
                children: [
                  imageFile != null
                      ? Container(
                          width: 80,
                          height: 80,
                          decoration: BoxDecoration(
                            border: Border.all(width: 4, color: Colors.white),
                            boxShadow: [
                              BoxShadow(
                                  spreadRadius: 2,
                                  blurRadius: 10,
                                  color: Colors.black.withOpacity(0.1),
                                  offset: const Offset(0, 10)),
                            ],
                            //shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                // ignore: unnecessary_null_comparison
                                image: FileImage(File(imageFile!.path))),
                          ),
                        )
                      : Container(),
                  Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: const BoxDecoration(),
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              imageFile = null;
                            });
                          },
                          child: const Icon(
                            Icons.cancel,
                            color: Colors.orange,
                          ),
                        ),
                      ))
                ],
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(
                bottom: 10.0, top: 15.0, left: 30.0, right: 30.0),
            child: MaterialButton(
              onPressed: () {
                addProduct();
              },
              color: Colors.orange,
              child: const Text(
                "SAVE",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              height: 36,
              minWidth: 130,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
          )
        ],
      ),
    );
  }

  Future getImage(ImageSource source) async {
    final imgpicker = await picker.getImage(source: source);
    setState(() {
      imageFile = imgpicker!;
      final bytes = File(imageFile!.path).readAsBytesSync();
      imageBase64 = base64Encode(bytes);
    });
  }

  void addProduct() {
    final product = Product()
      ..userId = user.email.toString()
      ..productImg = imageBase64.toString()
      ..productName = nameController.text
      ..storeName = widget.storeName
      ..rate = double.parse(rateController.text);

    final box = Boxes.getProduct();
    setState(() {
      box.add(product);

      NavigationUtils.navigateToDashboard(context);
    });
  }
}

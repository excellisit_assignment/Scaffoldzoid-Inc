import 'dart:convert';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:scaffoldzoid/login_screen.dart';
import 'package:scaffoldzoid/shared_pref.dart';

import 'boxes.dart';
import 'db_model.dart';
import 'navigation_utils.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  late final user = FirebaseAuth.instance.currentUser!;
  bool? isSeller;
  List<Seller> sellers = [];
  List<Product> products = [];
  List<Users> users = [];
  String storeName = "";

  List<Product> sellerProduct = [];

  Uint8List? bytes;
  String profileName = "";
  String description = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //isSeller =  SharedPreference.getBoolean('isSeller');
    getUser();
    getSeller();
    getProduct();
  }

  void getSeller() {
    sellers = Boxes.getSeller().values.toList().cast<Seller>();

    sellers.forEach((element) {
      if (element.userId == user.email) {
        bytes = base64Decode(element.profileImg);
        description = element.description;
        profileName = element.userName;
        storeName = element.storeName;
      }
    });
  }

  void getProduct() {
    products = Boxes.getProduct().values.toList().cast<Product>();
    products.forEach((element) {
      if (element.userId == user.email) {
        sellerProduct.add(element);
      }
    });
  }

  Future getUser() async {
    isSeller = await SharedPreference.getBoolean('isSeller');
    // print("isSeller:" + isSeller.toString());

    users = Boxes.getUsers().values.toList().cast<Users>();
    users.forEach((element) {
      if (element.isSeller != null && element.userId == user.email) {
        setState(() {
          isSeller = element.isSeller;
          print('isSeller:' + isSeller.toString());
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return isSeller!
        ? Scaffold(
            floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () {
                NavigationUtils.navigateToAddProduct(context, storeName);
              },
            ),
            appBar: AppBar(
              title: Text(storeName),
              centerTitle: true,
              //leading: ,
              actions: [
                IconButton(
                    icon: const Icon(Icons.logout),
                    onPressed: () {
                      // NavigationUtils.back(context);
                      setState(() {
                        FirebaseAuth.instance.signOut();
                        SharedPreference.clear();
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) {
                          return const LoginScreen();
                        }));
                      });
                    }),
              ],
            ),
            drawer: Drawer(
              child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: [
                  UserAccountsDrawerHeader(
                    decoration: const BoxDecoration(
                      color: Colors.orange,
                    ),
                    accountEmail: Text(
                      user.email.toString(),
                    ),
                    accountName: Text(profileName),
                    currentAccountPicture: bytes != null
                        ? CircleAvatar(
                            backgroundImage: MemoryImage(bytes!),
                          )
                        : const CircleAvatar(),
                  ),

                  // children: [Text('Welcome!!'),]

                  Column(
                    children: [
                      const Text(
                        "Description",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(description),
                    ],
                  ),
                  ListTile(
                    leading: const Icon(Icons.person),
                    title: const Text('Edit Profile'),
                    onTap: () {
                      NavigationUtils.navigateToProfile(context, user.email);
                    },
                  ),
                ],
              ),
            ),
            body: products.isEmpty
                ? const Center()
                : Column(
                    children: [
                      Expanded(
                        child: ListView.separated(
                          scrollDirection: Axis.vertical,
                          itemCount: sellerProduct.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                                elevation: 5.0,
                                margin: const EdgeInsets.all(2.0),
                                child: ListTile(
                                  title: Text(sellerProduct[index].productName),
                                  subtitle: Text(
                                      sellerProduct[index].rate.toString() +
                                          " /Kg"),
                                  trailing: Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 4, color: Colors.white),
                                          boxShadow: [
                                            BoxShadow(
                                                spreadRadius: 2,
                                                blurRadius: 10,
                                                color: Colors.black
                                                    .withOpacity(0.1),
                                                offset: const Offset(0, 10)),
                                          ],
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: MemoryImage(
                                              base64Decode(sellerProduct[index]
                                                  .productImg),
                                            ),
                                          ))),
                                  onTap: () {},
                                ));
                          },
                          separatorBuilder: (context, index) => const Divider(),
                        ),
                      )
                    ],
                  ),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(user.email.toString()),
              centerTitle: true,
              actions: [
                IconButton(
                    icon: const Icon(Icons.logout),
                    onPressed: () {
                      FirebaseAuth.instance.signOut();
                      SharedPreference.clear();
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) {
                        return const LoginScreen();
                      }));
                    }),
              ],
            ),
            body: products.isEmpty
                ? const Center()
                : Column(
                    children: [
                      Expanded(
                        child: ListView.separated(
                          scrollDirection: Axis.vertical,
                          itemCount: products.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                                elevation: 5.0,
                                margin: const EdgeInsets.all(2.0),
                                child: ListTile(
                                  title: Text(products[index].productName),
                                  subtitle: Text(
                                      products[index].rate.toString() + " /Kg"),
                                  leading: Text(products[index].storeName),
                                  trailing: Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 4, color: Colors.white),
                                          boxShadow: [
                                            BoxShadow(
                                                spreadRadius: 2,
                                                blurRadius: 10,
                                                color: Colors.black
                                                    .withOpacity(0.1),
                                                offset: const Offset(0, 10)),
                                          ],
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: MemoryImage(
                                              base64Decode(
                                                  products[index].productImg),
                                            ),
                                          ))),
                                  onTap: () {},
                                ));
                          },
                          separatorBuilder: (context, index) => const Divider(),
                        ),
                      )
                    ],
                  ),
          );
  }
}

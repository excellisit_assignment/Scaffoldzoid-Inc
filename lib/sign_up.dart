import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:scaffoldzoid/boxes.dart';
import 'package:scaffoldzoid/dashboard.dart';
import 'package:scaffoldzoid/db_model.dart';
import 'package:scaffoldzoid/main.dart';
import 'package:scaffoldzoid/navigation_utils.dart';
import 'package:hive/hive.dart';
import 'package:scaffoldzoid/shared_pref.dart';
import 'db_model.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController emailController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmController = TextEditingController();
  late final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();
  //bool val = false;
  bool checkVal = false;
  late final Box box;
  bool _passwordVisible = false, _confirmPasswordVisible = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    box = Boxes.getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text("Register a new membership"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          margin: const EdgeInsets.only(top: 8.0),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 15.0, top: 8.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  controller: emailController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 15.0),
                child: TextFormField(
                  obscureText: !_passwordVisible,
                  controller: newPasswordController,
                  decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: 'New Password',
                      suffixIcon: IconButton(
                        icon: Icon(
                          _passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                        onPressed: () {
                          setState(() {
                            _passwordVisible = !_passwordVisible;
                          });
                        },
                      )),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 15.0),
                child: TextFormField(
                  obscureText: !_confirmPasswordVisible,
                  controller: confirmController,
                  decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: 'Confirm Password',
                      suffixIcon: IconButton(
                        icon: Icon(
                          _confirmPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                        onPressed: () {
                          setState(() {
                            _confirmPasswordVisible = !_confirmPasswordVisible;
                          });
                        },
                      )),
                ),
              ),
              Row(
                children: [
                  Checkbox(
                    value: checkVal,
                    onChanged: (value) {
                      setState(() {
                        checkVal = value!;
                      });
                    },
                  ),
                  const Text(
                    "I am a seller",
                    style: TextStyle(fontSize: 18),
                  )
                ],
              ),
              MaterialButton(
                onPressed: () => signUp(),
                color: Colors.orange,
                child: const Text(
                  "SUBMIT",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
                height: 36,
                minWidth: MediaQuery.of(context).size.width / 2.5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future signUp() async {
    if (newPasswordController.text == confirmController.text) {
      try {
        await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
                email: emailController.text.trim(),
                password: newPasswordController.text.trim())
            .then((value) {
          final users = Users()
            ..userId = emailController.text
            ..isSeller = checkVal;
          setState(() {
            box.add(users);
            SharedPreference.putBoolean('isSeller', checkVal);
          });
        });

        // await addUser();
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) {
          return const Dashboard();
        }));
      } catch (e) {
        Fluttertoast.showToast(
            msg: "Something went wrong", backgroundColor: Colors.grey);
      }
    } else {
      Fluttertoast.showToast(
          msg: "Passwords didn't match", backgroundColor: Colors.grey);
    }
  }
}

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:scaffoldzoid/add_product.dart';
import 'package:scaffoldzoid/dashboard.dart';
import 'package:scaffoldzoid/profile.dart';
import 'package:scaffoldzoid/sign_up.dart';

class NavigationUtils {
  static void back(BuildContext context) {
    Navigator.of(context).pop();
  }

  static void navigateToSignUp(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: const SignUp()));
  }

  static void navigateToDashboard(BuildContext context) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft, child: const Dashboard()));
  }

  static void navigateToProfile(BuildContext context, String? userID) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: Profile(userId: userID!)));
  }

  static void navigateToAddProduct(BuildContext context, String storeName) {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.rightToLeft,
            child: AddProduct(
              storeName: storeName,
            )));
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:scaffoldzoid/boxes.dart';
import 'package:scaffoldzoid/db_model.dart';
import 'package:scaffoldzoid/navigation_utils.dart';
import 'package:scaffoldzoid/shared_pref.dart';
import 'package:scaffoldzoid/sign_up.dart';
import 'boxes.dart';
import 'dashboard.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController? emailController = TextEditingController();
  TextEditingController? passwordController = TextEditingController();
  bool checkVal = false;
  List<Users> users = [];

  bool _passwordVisible = false;

  void initState() {
    // TODO: implement initState
    super.initState();
    users = Boxes.getUsers().values.toList().cast<Users>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text("Scaffold Inc."),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              padding: const EdgeInsets.only(top: 2),
              width: 300,
              height: 150,
              decoration: const BoxDecoration(
                color: Colors.white38,
                shape: BoxShape.circle,
              ),
              child: Image.asset(
                "assets/images/orange_342874121.jpeg",
                width: 100,
                height: 100,
              )),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 8, 40, 8),
            child: TextField(
              style: const TextStyle(color: Colors.black),
              controller: emailController,
              decoration: const InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  labelText: 'Email',
                  labelStyle: TextStyle(color: Colors.black),
                  counterText: ''),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 8, 40, 8),
            child: TextField(
              style: const TextStyle(color: Colors.black),
              obscureText: !_passwordVisible,
              controller: passwordController,
              decoration: InputDecoration(
                  enabledBorder: const UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  labelText: 'Password',
                  labelStyle: const TextStyle(color: Colors.black),
                  suffixIcon: IconButton(
                    icon: Icon(
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    },
                  )),
            ),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            const SizedBox(width: 20),
            MaterialButton(
              onPressed: () {
                login();
                //  NavigationUtils.navigateToDashboard(context);
              },
              color: Colors.orange,
              child: const Text(
                "LOGIN",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              height: 36,
              minWidth: 130,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
            const SizedBox(
              width: 40,
            ),
            Checkbox(
              value: checkVal,
              onChanged: (value) {
                setState(() {
                  checkVal = value!;
                });
              },
            ),
            const Text(
              "I am a seller",
              style: TextStyle(fontSize: 18),
            ),
          ]),
          const Padding(
            padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 20),
            child: Text("---------- OR ----------",
                style: TextStyle(color: Colors.black)),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 15),
            child: MaterialButton(
              elevation: 100.0,
              color: Colors.transparent,
              onPressed: () {
                NavigationUtils.navigateToSignUp(context);
              },
              child: const Text(
                "CREATE YOUR STORE/SIGN UP AS A BUYER",
                style: TextStyle(
                    color: Colors.orange, fontWeight: FontWeight.bold),
              ),
              shape: RoundedRectangleBorder(
                  side: const BorderSide(color: Colors.orange),
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
        ],
      ),
    );
  }

  Future login() async {
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (context) => const Center(
    //           child: CircularProgressIndicator(),
    //         ));
    // NavigationUtils.back(context);
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: emailController!.text.trim(),
              password: passwordController!.text.trim())
          .then((value) => {
                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                  return const Dashboard();
                })),
                users.forEach((element) {
                  if (element.userId == value.user!.email) {
                    SharedPreference.putBoolean('isSeller', element.isSeller);
                  } else {
                    SharedPreference.putBoolean('isSeller', checkVal);
                  }
                })
              });
    } catch (e) {
      Fluttertoast.showToast(
          msg: "Invalid user and password", backgroundColor: Colors.grey);
    }
  }
}

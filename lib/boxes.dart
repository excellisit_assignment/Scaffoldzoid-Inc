import 'package:hive/hive.dart';
import 'db_model.dart';

class Boxes {
  static Box<Users> getUsers() => Hive.box<Users>('users');
  static Box<Seller> getSeller() => Hive.box<Seller>('seller');
  static Box<Product> getProduct() => Hive.box<Product>('product');
}

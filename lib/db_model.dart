import 'package:hive/hive.dart';
part 'db_model.g.dart';

@HiveType(typeId: 0)
class Users extends HiveObject {
  @HiveField(0)
  late String userId;
  @HiveField(1)
  late bool isSeller;
}

@HiveType(typeId: 1)
class Seller extends HiveObject {
  @HiveField(0)
  late String userId;
  @HiveField(1)
  late String userName;
  @HiveField(2)
  late String storeName;
  @HiveField(3)
  late String description;
  @HiveField(4)
  late String profileImg;
}

@HiveType(typeId: 2)
class Product extends HiveObject {
  @HiveField(0)
  late String productName;
  @HiveField(1)
  late double rate;
  @HiveField(2)
  late String productImg;
  @HiveField(3)
  late String userId;
  @HiveField(4)
  late String storeName;
}

import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scaffoldzoid/db_model.dart';
import 'boxes.dart';
import 'navigation_utils.dart';
import 'package:flutter/services.dart';

class Profile extends StatefulWidget {
  final String? userId;
  const Profile({
    Key? key,
    required this.userId,
  }) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final picker = ImagePicker();
  PickedFile? imageFile;
  String? imageBase64;
  bool isImage = false;
  TextEditingController? storeNameController = TextEditingController();
  TextEditingController? userNameController = TextEditingController();
  TextEditingController? descriptionController = TextEditingController();

  List<Seller> sellers = [];

  int? userKey;
  Uint8List? bytes;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sellers = Boxes.getSeller().values.toList().cast<Seller>();

    sellers.forEach((element) {
      if (element.userId == widget.userId) {
        storeNameController!.text = element.storeName;
        userNameController!.text = element.userName;
        descriptionController!.text = element.description;
        userKey = element.key;
        bytes = base64Decode(element.profileImg);
        isImage = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    imageCache!.clear();
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: const Text("Edit Profile"),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.only(top: 25, left: 10, right: 10),
        child: ListView(
          children: [
            imageProfile(),
            Container(
              margin: const EdgeInsets.only(
                  bottom: 10.0, top: 50.0, left: 8.0, right: 8.0),
              child: TextFormField(
                controller: storeNameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Store Name',
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                  bottom: 10.0, top: 10.0, left: 8.0, right: 8.0),
              child: TextFormField(
                controller: userNameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'User name',
                ),
              ),
            ),
            Container(
              height: 100,
              margin: const EdgeInsets.only(
                  bottom: 10.0, top: 10.0, left: 8.0, right: 8.0),
              child: TextFormField(
                controller: descriptionController,
                maxLines: 5,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Description',
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                  bottom: 10.0, top: 15.0, left: 30.0, right: 30.0),
              child: MaterialButton(
                onPressed: () {
                  saveProfile();
                },
                color: Colors.orange,
                child: const Text(
                  "SAVE",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
                height: 36,
                minWidth: 130,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget bottomPane() {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: [
          const Text("Choose Profile Photo"),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // ignore: prefer_const_constructors
              TextButton(
                  onPressed: () {
                    getImage(ImageSource.camera);
                  },
                  child: Row(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: const [
                      Icon(
                        Icons.camera,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text("Camera")
                    ],
                  )),
              const SizedBox(
                width: 15,
              ),
              TextButton(
                  onPressed: () {
                    getImage(ImageSource.gallery);
                  },
                  child: Row(
                    children: const [
                      Icon(
                        Icons.image,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text("Gallery")
                    ],
                  ))
            ],
          )
        ],
      ),
    );
  }

  Widget imageProfile() {
    return Center(
      child: Stack(
        children: [
          bytes != null
              ? Container(
                  width: 130,
                  height: 130,
                  decoration: BoxDecoration(
                    border: Border.all(width: 4, color: Colors.white),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 2,
                          blurRadius: 10,
                          color: Colors.black.withOpacity(0.1),
                          offset: const Offset(0, 10)),
                    ],
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: !isImage
                            ? FileImage(File(imageFile!.path)) as ImageProvider
                            : MemoryImage(bytes!)),
                  ),
                )
              : Container(
                  width: 130,
                  height: 130,
                  decoration: BoxDecoration(
                    border: Border.all(width: 4, color: Colors.white),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 2,
                          blurRadius: 10,
                          color: Colors.black.withOpacity(0.1),
                          offset: const Offset(0, 10)),
                    ],
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: imageFile != null
                            ? FileImage(File(imageFile!.path)) as ImageProvider
                            : const AssetImage(
                                'assets/images/orange_342874121.jpeg')),
                  ),
                ),
          Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(),
                child: GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                        context: context, builder: (builder) => bottomPane());
                  },
                  child: const Icon(
                    Icons.camera_alt,
                    color: Colors.teal,
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Future getImage(ImageSource source) async {
    final imgpicker = await picker.getImage(source: source);
    setState(() {
      isImage = false;
      imageFile = imgpicker!;
      final bytes = File(imageFile!.path).readAsBytesSync();
      imageBase64 = base64Encode(bytes);
    });
  }

  void saveProfile() {
    final seller = Seller()
      ..storeName = storeNameController!.text
      ..description = descriptionController!.text
      ..userName = userNameController!.text
      ..profileImg = imageBase64.toString()
      ..userId = widget.userId!;

    print("seller" + seller.toString());

    final box = Boxes.getSeller();
    setState(() {
      if (userKey == null) {
        box.add(seller);
      } else {
        box.put(userKey, seller);
      }

      NavigationUtils.navigateToDashboard(context);
    });
  }
}
